/*******************
	Include
*******************/

#include<unistd.h>
#include<stdlib.h>
#include<stdio.h>
#include<pthread.h>
#include<semaphore.h>

/**** Declaration of global Variables ****/

#define SIZE 5
int queue [SIZE];
int rear = -1,front = -1;

sem_t a;

void *producer (void *arg);		/**** Function 1 Declaration ****/
void *consumer (void *arg1);		/**** Function 2 Declaration ****/

/**** Main ****/

int main()
{
	int p,q,r;
	pthread_t x,y;
	/*** Semaphore Initialization ***/
	
	r = sem_init(&a,0,1);
	if (r == 0) 
	{
		printf("Semaphore initialization Successfull.\n");
	}
	
	/**** THREAD 1 CREATION ****/
	
	p = pthread_create (&x, NULL, producer, NULL);
	pthread_join(x, NULL);
	
	/**** THREAD 2 CREATION ****/
	
	q = pthread_create (&y, NULL, consumer, NULL);
	pthread_join(y, NULL);
	
	if (p == q == 0 ) 
	{
		printf("Thread creation successfull.\n");
	}
	
	sem_destroy(&a);
	printf("\n");
	return 0;
}
	
	
/**** Function 1 Initialization ****/
	
void *producer (void *arg)
{
	sem_wait (&a);
	int z;
	if(rear == SIZE - 4)
	{
		printf("overflow\n");
	}
	else
	{
		if(front == -1)
		{
			front = 0;
			printf("Enter data:-");
			scanf("%d",&z);
			rear = rear+1;
			queue[rear]=z;
		}
	}
	sem_post(&a);
	
}


/**** Function 2 Initialization ****/
	
void *consumer (void *arg1)
{
	sem_wait(&a);
	if(front == -1)
	{
		printf("EMPTY\n");
	}
	else
	{
		printf("Queue:- %d\n",queue[front]);
	}
	sem_post(&a);
}












		
	
	
	
	
	
