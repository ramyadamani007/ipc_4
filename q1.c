/*******************
	Include
*******************/

#include<unistd.h>
#include<stdlib.h>
#include<stdio.h>
#include<string.h>

/**** Main ****/

int main()
{
	int a,b;
	int file_pipes[2];	
	const char p[2];
	char buffer[BUFSIZ + 1];
	
	printf("Enter any integer.\n");
	scanf("%s",&p);
	
	if (pipe(file_pipes) == 0)
	{
		a = write (file_pipes[1], p, strlen(p));
		printf("Wrote %d bytes\n", a);
		
		a = read(file_pipes[0], buffer, BUFSIZ);
		printf("Read %d bytes: %s\n", a, buffer);
		exit(EXIT_SUCCESS);
	}
	exit(EXIT_FAILURE);
	return 0;
}
	
	
	
	

	
	
	
	
	
