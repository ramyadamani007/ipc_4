/*******************
	Include
*******************/

#include<unistd.h>
#include<stdlib.h>
#include<stdio.h>
#include<string.h>
#include<sys/types.h>
#include<sys/stat.h>
#include<fcntl.h>

/**** Main ****/

int main()
{
	int fd[2];
	char str[50],str1[50];
	pipe(fd);
	if(fork()!=0)
	{
		close(fd[0]);
		printf("enter a string\n");
		scanf("%s",str);
		write(fd[1],str,sizeof(str));
		printf("send:- %s\n",str);
		close(fd[1]);
	}
	else
	{
		close(fd[1]);
		read(fd[0],str1,sizeof(str1));
		printf("Received string by Child process is %s\n",str1);
		close(fd[0]);
	}
}
