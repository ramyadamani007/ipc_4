/*******************
	Include
*******************/

#include<unistd.h>
#include<stdlib.h>
#include<stdio.h>
#include<string.h>
#include<sys/types.h>
#include<sys/stat.h>
#include<fcntl.h>

/**** Main ****/

int main()
{
	int fd,p,q,fz;
	char str[50] = "I am Intern at Scaledge India Pvt.Ltd.",arr[50];
	FILE *fr, *fp;
	fr = fopen ("file1_q4.txt","a");
	fp = fopen ("file2_q4.txt","a");
	
	if (fr == NULL && fp == NULL)
	{
		printf("File is not open.\n");
		exit (1);
	}
	
	
	char *file1_q4 = "/ipc_5/file1_q4";
	p = mkfifo ("file1_q4.txt", 0777);
	printf("FILE 1 created.\n");
	
	char *file2_q4 = "/ipc_5/file2_q4";
	q = mkfifo ("file2_q4.txt", 0777);
	printf("FILE 2 created.\n");
	
	if (p == 0 && q == 0)
	{
		printf("The fifo is created.\n");
		exit (EXIT_SUCCESS);
	} 

	fd = open ("file1_q4.txt",O_WRONLY);
	write (fd, str, sizeof (str));
	printf("data written in file1_q4.txt successfully.\n");
	fz = open ("file2_q4.txt",O_WRONLY | O_NONBLOCK);
	strcpy(arr,str);
	memcpy(arr, str, strlen(str));
	write (fd, arr, sizeof (arr));
	printf("data copied in file2_q4.txt successfully.\n");
	close (fz);
	close (fd);
	return 0;
}
	
	
	
	
	
	
	
	
