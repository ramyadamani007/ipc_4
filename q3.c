/*******************
	Include
*******************/

#include<stdio.h>
#include<unistd.h>
#include<sys/shm.h>
#include<string.h>
#include<unistd.h>
#include<stdlib.h>

#define input_str 2048
#define TRUE 1
#define FALSE 0

/*** structure which is shared memory across two process.***/

struct shared_use
{
	int data;		/** Variable which indicates available data **/
	char msg[input_str];	/** Char array to hold input_string**/
};

/**** Main ****/

int main()
{
	int process_running = TRUE;
	void *shared_memory = (void *)0;
	
	// Structure which acts like shared memory //
	
	struct shared_use *shared_stuff;
	char buffer [BUFSIZ];
	int shmid;
	
	// Allocation of shared memory //
	
	shmid = shmget (1234, sizeof (struct shared_use),0666 | IPC_CREAT);
	
	// On failure shmgit () returns -1 //
	
	if (shmid == -1)
	{
		fprintf(stderr,"shmget failed.\n");
		exit (EXIT_FAILURE);
	}
	
	// Attachment of Segment //
	
	shared_memory = shmat (shmid, (void *)0, 0);
	
	// on failure shmat () returns -1 //
	
	if (shared_memory == (void*)-1)
	{
		fprintf(stderr,"shmat failed.\n");
		exit (EXIT_FAILURE);
	}
	
	// on successfull attachment of segment //
	
	printf("Memory attached at %x\n", shared_memory);
	shared_stuff = (struct shared_use *)shared_memory;
	
	// process is performing to write operation //
	while (process_running)
	{
		// other process is still reading.//
		while (shared_stuff->data == TRUE)
		{
			// waits for other process to complete its task. //
			printf("Waiting for client....\n");
			sleep (3);
		}
		
		// server creates string by asking user an input. //
		printf("Enter some text: ");
		
		// reads the input and stores it in buffer //
		fgets(buffer, BUFSIZ, stdin);
		
		// writes it into char array msg[[]
		strncpy(shared_stuff-> msg, buffer, input_str);
		
		//checks if data is available //
		shared_stuff -> data = TRUE;
		
		// string 'end' is use to terminate the loop //
		if (strncmp(buffer, "end", 3) == FALSE)
		{
			// Indicates the data is written and sets the flag.//
			process_running = FALSE;
		}
	}
	
	// Detachment of segment //
	if (shmdt (shared_memory) == -1)
	{
		fprintf(stderr, "shmdt failed.\n");
		exit (EXIT_FAILURE);
	}
	// end of server program //

	return 0;
}
	
		
		
		
		
	
	
	
	
	
	
	
	
