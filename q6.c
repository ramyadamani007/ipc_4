/*******************
	Include
*******************/

#include<unistd.h>
#include<stdlib.h>
#include<stdio.h>
#include<string.h>
#include<sys/types.h>
#include<sys/stat.h>
#include<fcntl.h>

struct details
{
	char name[15];
	char add[50];
	int no;
}d1,d2;


/**** Main ****/

int main()
{
	struct details d1 = {"Ramya Damani","Mahila college circle, Bhavnagar",9999999999},d2;
	int  fd[2];
	pipe(fd);
	if(fork()!=0)
	{
		close(fd[0]);
		write(fd[1],&d1,sizeof(d1));
		printf("struct included successfully\n");
		close(fd[1]);
	}
	else
	{
		close(fd[1]);
		read(fd[0],&d2,sizeof(d2));
		printf("struct found\n");
		printf("Name: %s\n",d2.name);
		printf("Address: %s\n",d2.add);
		printf("Mobile NO:-%d\n",d2.no);
		printf("\n");
		close(fd[0]);
	}
}
	
